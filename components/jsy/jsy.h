#ifndef JSY
#define JSY

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "driver/uart.h"
#include "string.h"
#include "driver/gpio.h"
#include <inttypes.h>

#define TXD_PIN (GPIO_NUM_4)
#define RXD_PIN (GPIO_NUM_5)

static const int RX_BUF_SIZE = 128;
static const int TX_BUF_SIZE = 128;

uint8_t check_crc(uint8_t*, const size_t);
void    compute_crc(uint8_t*, const size_t, uint8_t*);
float   get_current(const uart_port_t, const uint8_t, uint8_t*, const uint8_t, const uint8_t);
float   get_frequency(const uart_port_t, const uint8_t, uint8_t*, const uint8_t);
int     get_jsy_speed(const uart_port_t, const uint8_t);
uint8_t get_measure(const uart_port_t, const uint8_t, uint8_t*);
float   get_negative_energy(const uart_port_t, const uint8_t, uint8_t*, const uint8_t, const uint8_t);
float   get_positive_energy(const uart_port_t, const uint8_t, uint8_t*, const uint8_t, const uint8_t);
float   get_power(const uart_port_t, const uint8_t, uint8_t*, const uint8_t, const uint8_t);
float   get_power_factor(const uart_port_t, const uint8_t, uint8_t*, const uint8_t, const uint8_t);
float   get_voltage(const uart_port_t, const uint8_t, uint8_t*, const uint8_t, const uint8_t);
int     read_jsy_reg(const uart_port_t, const uint8_t, const uint16_t, const uint16_t, uint8_t*);
uint8_t reset_negative_energy(const uart_port_t, const uint8_t, const int);
uint8_t reset_positive_energy(const uart_port_t, const uint8_t, const int);
int     send_data(const uart_port_t, const uint8_t*, const size_t);
void    set_esp_uart_speed(uart_port_t, int);
uint8_t set_jsy_speed(const uart_port_t, const uint8_t, const int);
void    setup_esp_uart(const uart_port_t, const int, const int);
int     write_jsy_reg(const uart_port_t, const uint8_t, const uint16_t, const uint16_t, const uint8_t, uint8_t*, uint8_t*);

#endif