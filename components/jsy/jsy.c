#include "jsy.h"


/**
 * \brief Cette fonction vérifie le CRC.
 * 
 * \param[in] bytes Tableau d'octets contenant les données dont il faut vérifier
 *                  le CRC
 * \param[in] nb_bytes Nombre d'octets à prendre en compte dans le tableau
 *                     ci-dessus pour le calcul du CRC
 * \return 1 si le CRC est valide, 0 sinon.
*/
uint8_t check_crc(uint8_t* bytes, const size_t nb_bytes) {
    
    // Tableau pour les 2 octets de CRC
    uint8_t crc[2];

    // On calcule le CRC
    compute_crc(bytes, nb_bytes, crc);

    // Si les 2 octets suivant les données correspondent bien au CRC, c'est OK !
    if (bytes[nb_bytes] == crc[0] && bytes[nb_bytes+1] == crc[1]) {
        return 1;
    }

    return 0;
}

/**
 * \brief Cette fonction calcule le CRC de la manière spécifique au module JSY.
 * 
 * \param[in] bytes Tableau d'octets contenant les données dont il faut calculer
 *                  le CRC
 * \param[in] nb_bytes Nombre d'octets à prendre en compte dans le tableau
 *                     ci-dessus pour le calcul du CRC
 * \param[out] out_crc Tableau d'octets pour retourner le CRC
*/
void
compute_crc(uint8_t* bytes, const size_t nb_bytes, uint8_t* out_crc) {
    uint16_t crc = 0xFFFF;
    for (size_t processed_bytes=0;processed_bytes < nb_bytes;processed_bytes++) {
        crc ^= (uint16_t) bytes[processed_bytes];
        for (uint8_t i = 0; i < 8; i++) {
            if ((crc & 0x0001) == 0x0001) {
                crc >>= 1;
                crc ^= 0xA001;
            }
            else {
                crc >>= 1;
            }
        }
    }
    out_crc[0] = crc & 0x00FF;;
    out_crc[1] = (crc >> 8) & 0x00FF;
}

/**
 * \brief Cette fonction permet de connaitre le courant d'un canal.
 * 
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont on veut obtenir le courant.
 * \param[in] rx_data Tableau d'octets pour stocker les données électriques.
 * \param[in] channel Numéro du canal pour lequel on souhaite avoir le courant.
 * \param[in] update Si cet argument vaut 0, il n'y a pas de nouvelle mesure
 *                   effectuée et le courant est extrait des données déjà
 *                   présentes dans rx_data.
 * \return Courant en ampères
*/
float
get_current(const uart_port_t uart_num, const uint8_t dev_addr, uint8_t* rx_data, const uint8_t channel, const uint8_t update) {

    // Index du premier élément du tableau d'octet contenant les données de
    // courant
    uint8_t idx;

    // Tension
    uint32_t current = 0;

    // On force une nouvelle mesure si besoin
    if (update)
    {
        if (! get_measure(uart_num, dev_addr,rx_data))
        {
            // Si aucune donnée n'est retournée, on sort avec un code d'erreur
            return -1;
        }
    }

    // Cherchons la valeur de idx en fonction du canal souhaité
    if (channel == 1)
    {
        idx = 7;
    }
    else if (channel == 2)
    {
        idx = 39;
    }
    else
    {
        // Numéro de canal invalide
        return -1;
    }

    // printf("Current data: %02x %02x %02x %02x\n", rx_data[idx], rx_data[idx+1], rx_data[idx+2], rx_data[idx+3]);
    current = (rx_data[idx] << 24) | (rx_data[idx+1] << 16) | (rx_data[idx+2] << 8) | rx_data[idx+3];
    // printf("Current data: %08" PRIx32 "\n", voltage);
    
    return current / 10000.0;
}

/**
 * \brief Cette fonction permet de connaitre la fréquence d'un canal.
 * 
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont on veut obtenir la fréquence.
 * \param[in] rx_data Tableau d'octets pour stocker les données électriques.
 * \param[in] update Si cet argument vaut 0, il n'y a pas de nouvelle mesure
 *                   effectuée et la fréquence est extraite des données déjà
 *                   présentes dans rx_data.
 * \return fréquence en hertz.
*/
float
get_frequency(const uart_port_t uart_num, const uint8_t dev_addr, uint8_t* rx_data, const uint8_t update) {

    // Index du premier élément du tableau d'octet contenant les données de
    // fréquence
    uint8_t idx = 31;

    // Fréquence
    uint32_t frequency = 0;

    // On force une nouvelle mesure si besoin
    if (update)
    {
        if (! get_measure(uart_num, dev_addr,rx_data))
        {
            // Si aucune donnée n'est retournée, on sort avec un code d'erreur
            return -1;
        }
    }

    // printf("Voltage data: %02x %02x %02x %02x\n", rx_data[idx], rx_data[idx+1], rx_data[idx+2], rx_data[idx+3]);
    frequency = (rx_data[idx] << 24) | (rx_data[idx+1] << 16) | (rx_data[idx+2] << 8) | rx_data[idx+3];
    // printf("Voltage data: %08" PRIx32 "\n", voltage);
    
    return frequency / 100.0;
}

/**
 * \brief Cette fonction détermine la vitesse actuelle d'un module JSY.
 * 
 * Pour ce faire, elle va tenter de communiquer avec le module JSY via un UART à
 * différentes vitesses en commençant par la vitesse la plus élevée, jusqu'à ce
 * que l'on obtienne une réponse valide du JSY.
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont il faut déterminer la vitesse.
 * \return Vitesse actuelle de l'UART du module JSY en bauds.
 */
int
get_jsy_speed(const uart_port_t uart_num, const uint8_t dev_addr) {

    // Tableau des différentes vitesses possibles
    int speeds[] = {38400, 19200, 9600, 4800, 2400, 1200};

    // Tableau des différentes valeurs du dernier octet du registre associées
    // aux vitesses du tableau ci-dessus.
    uint8_t speed_vals[] = {8, 7, 6, 5, 4, 3};

    // Tableau d'octets pour stocket la réponse du module JSY
    uint8_t rx_data[RX_BUF_SIZE];

    for (size_t i = 0; i < 6; i++)
    {

        int speed = speeds[i];

        printf("Let's try %d bauds\n", speed);

        // On commence par configurer l'UART de l'ESP avec cette vitesse
        set_esp_uart_speed(uart_num, speed);

        // On essaie de lire le registre à l'adresse 0x0004
        const int rx_bytes = read_jsy_reg(uart_num, dev_addr, 0x0004, 1, rx_data);
        if (rx_bytes > 0) {

            // On y est presque, il faut encore vérifier la valeur du dernier
            // octet du registre pour voir s'il correspond bien à la vitesse
            // supposée
            if (rx_data[rx_bytes-1] == speed_vals[i]) {
                printf("Current speed is %d bauds\n", speeds[i]);
                return speed;
            }
        }

    }
    
    /* Si on n'est pas sorti plus tôt, c'est qu'on n'a pas réussi à déterminer
     * la vitesse. */
    return 0;
}

/**
 * \brief Cette fonction récupère les données de tous les registres relatifs aux
 *        mesures électriques.
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont on veut obtenir une mesure.
 * \param[in] rx_data Tableau d'octets pour stocker les données électriques
 * \return Nombre d'octets
*/
uint8_t
get_measure(const uart_port_t uart_num, const uint8_t dev_addr, uint8_t* rx_data) {

    int nb_bytes = read_jsy_reg(uart_num, dev_addr, 0x00048, 14, rx_data);

    if (nb_bytes > 0) {
        return (uint8_t) nb_bytes;
    }

    return 0;

}

/**
 * \brief Cette fonction permet de connaitre le cumul d'énergie négative d'un canal.
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont on veut obtenir le cumul
 *                     d'énergie négative.
 * \param[in] rx_data Tableau d'octets pour stocker les données électriques.
 * \param[in] channel Numéro du canal pour lequel on souhaite avoir le cumul 
 *                    d'énergie négative.
 * \param[in] update Si cet argument vaut 0, il n'y a pas de nouvelle mesure
 *                   effectuée et le cumul d'énergie négative est extrait des
 *                   données déjà présentes dans rx_data.
 * \return Energie en kWh
*/
float
get_negative_energy(const uart_port_t uart_num, const uint8_t dev_addr, uint8_t* rx_data, const uint8_t channel, const uint8_t update) {

    // Index du premier élément du tableau d'octet contenant les données de
    // cumul d'énergie négative
    uint8_t idx;

    // Cumul d'énergie
    uint32_t energy = 0;

    // On force une nouvelle mesure si besoin
    if (update)
    {
        if (! get_measure(uart_num, dev_addr,rx_data))
        {
            // Si aucune donnée n'est retournée, on sort avec un code d'erreur
            return -1;
        }
    }

    // Cherchons la valeur de idx en fonction du canal souhaité
    if (channel == 1)
    {
        idx = 23;
    }
    else if (channel == 2)
    {
        idx = 55;
    }
    else
    {
        // Numéro de canal invalide
        return -1;
    }

    // printf("Current data: %02x %02x %02x %02x\n", rx_data[idx], rx_data[idx+1], rx_data[idx+2], rx_data[idx+3]);
    energy = (rx_data[idx] << 24) | (rx_data[idx+1] << 16) | (rx_data[idx+2] << 8) | rx_data[idx+3];
    // printf("Current data: %08" PRIx32 "\n", voltage);
    
    return energy / 10000.0;
}

/**
 * \brief Cette fonction permet de connaitre le cumul d'énergie positive d'un canal.
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont on veut obtenir le cumul
 *                     d'énergie positive.
 * \param[in] rx_data Tableau d'octets pour stocker les données électriques.
 * \param[in] channel Numéro du canal pour lequel on souhaite avoir le cumul 
 *                    d'énergie positive.
 * \param[in] update Si cet argument vaut 0, il n'y a pas de nouvelle mesure
 *                   effectuée et le cumul d'énergie positive est extrait des
 *                   données déjà présentes dans rx_data.
 * \return Energie en kWh
*/
float
get_positive_energy(const uart_port_t uart_num, const uint8_t dev_addr, uint8_t* rx_data, const uint8_t channel, const uint8_t update) {

    // Index du premier élément du tableau d'octet contenant les données de
    // cumul d'énergie positive
    uint8_t idx;

    // Tension
    uint32_t energy = 0;

    // On force une nouvelle mesure si besoin
    if (update)
    {
        if (! get_measure(uart_num, dev_addr,rx_data))
        {
            // Si aucune donnée n'est retournée, on sort avec un code d'erreur
            return -1;
        }
    }

    // Cherchons la valeur de idx en fonction du canal souhaité
    if (channel == 1)
    {
        idx = 15;
    }
    else if (channel == 2)
    {
        idx = 47;
    }
    else
    {
        // Numéro de canal invalide
        return -1;
    }

    // printf("Current data: %02x %02x %02x %02x\n", rx_data[idx], rx_data[idx+1], rx_data[idx+2], rx_data[idx+3]);
    energy = (rx_data[idx] << 24) | (rx_data[idx+1] << 16) | (rx_data[idx+2] << 8) | rx_data[idx+3];
    // printf("Current data: %08" PRIx32 "\n", voltage);
    
    return energy / 10000.0;
}

/**
 * \brief Cette fonction permet de connaitre la puissance d'un canal.
 * 
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont on veut obtenir la puissance.
 * \param[in] rx_data Tableau d'octets pour stocker les données électriques.
 * \param[in] channel Numéro du canal pour lequel on souhaite avoir la
 *                    puissance.
 * \param[in] update Si cet argument vaut 0, il n'y a pas de nouvelle mesure
 *                   effectuée et la puissance est extraite des données déjà
 *                   présentes dans rx_data.
 * \return Puissance en W
*/
float
get_power(const uart_port_t uart_num, const uint8_t dev_addr, uint8_t* rx_data, const uint8_t channel, const uint8_t update) {

    // Index du premier élément du tableau d'octets contenant les données de
    // puissance
    uint8_t idx;

    // Index de l'élément du tableau d'octets contenant la donnée du sens de la
    // puissance.
    uint8_t idx_dir;

    // Tension
    uint32_t power = 0;

    // Direction de la puissance
    uint8_t power_dir;

    // On force une nouvelle mesure si besoin
    if (update)
    {
        if (! get_measure(uart_num, dev_addr,rx_data))
        {
            // Si aucune donnée n'est retournée, on sort avec un code d'erreur
            return -1;
        }
    }

    // Cherchons la valeur de idx en fonction du canal souhaité
    if (channel == 1)
    {
        idx = 11;
        idx_dir = 27;
    }
    else if (channel == 2)
    {
        idx = 43;
        idx_dir = 28;
    }
    else
    {
        // Numéro de canal invalide
        return -1;
    }

    // printf("Current data: %02x %02x %02x %02x\n", rx_data[idx], rx_data[idx+1], rx_data[idx+2], rx_data[idx+3]);
    power = (rx_data[idx] << 24) | (rx_data[idx+1] << 16) | (rx_data[idx+2] << 8) | rx_data[idx+3];
    // printf("Current data: %08" PRIx32 "\n", voltage);

    if (rx_data[idx_dir] == 0)
    {
        power_dir = 1;
    }
    else if (rx_data[idx_dir] == 1)
    {
        power_dir = -1;
    }
    else
    {
        return -1;
    }
    

    return power * power_dir / 10000.0;
}

/**
 * \brief Cette fonction permet de connaitre le facteur de puissance d'un canal.
 * 
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont on veut obtenir le facteur de
 *                     puissance.
 * \param[in] rx_data Tableau d'octets pour stocker les données électriques.
 * \param[in] channel Numéro du canal pour lequel on souhaite avoir le facteur
 *                    de puissance.
 * \param[in] update Si cet argument vaut 0, il n'y a pas de nouvelle mesure
 *                   effectuée et le facteur de puissance est extrait des
 *                   données déjà présentes dans rx_data.
 * \return Facteur de puissance
*/
float
get_power_factor(const uart_port_t uart_num, const uint8_t dev_addr, uint8_t* rx_data, const uint8_t channel, const uint8_t update) {

    // Index du premier élément du tableau d'octets contenant les données de
    // facteur de puissance
    uint8_t idx;

    // Facteur de puissance
    uint32_t power_factor = 0;

    // On force une nouvelle mesure si besoin
    if (update)
    {
        if (! get_measure(uart_num, dev_addr,rx_data))
        {
            // Si aucune donnée n'est retournée, on sort avec un code d'erreur
            return -1;
        }
    }

    // Cherchons la valeur de idx en fonction du canal souhaité
    if (channel == 1)
    {
        idx = 19;
    }
    else if (channel == 2)
    {
        idx = 51;
    }
    else
    {
        // Numéro de canal invalide
        return -1;
    }

    // printf("Current data: %02x %02x %02x %02x\n", rx_data[idx], rx_data[idx+1], rx_data[idx+2], rx_data[idx+3]);
    power_factor = (rx_data[idx] << 24) | (rx_data[idx+1] << 16) | (rx_data[idx+2] << 8) | rx_data[idx+3];
    // printf("Current data: %08" PRIx32 "\n", voltage);

    return power_factor / 1000.0;
}

/**
 * \brief Cette fonction permet de connaitre la tension d'un canal.
 * 
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont on veut obtenir une tension.
 * \param[in] rx_data Tableau d'octets pour stocker les données électriques.
 * \param[in] channel Numéro du canal pour lequel on souhaite avoir la tension.
 * \param[in] update Si cet argument vaut 0, il n'y a pas de nouvelle mesure
 *                   effectuée et la tension est extraite des données déjà
 *                   présentes dans rx_data.
 * \return Tension en volts.
*/
float
get_voltage(const uart_port_t uart_num, const uint8_t dev_addr, uint8_t* rx_data, const uint8_t channel, const uint8_t update) {

    // Index du premier élément du tableau d'octet contenant les données de
    // tension
    uint8_t idx;

    // Tension
    uint32_t voltage = 0;

    // On force une nouvelle mesure si besoin
    if (update)
    {
        if (! get_measure(uart_num, dev_addr,rx_data))
        {
            // Si aucune donnée n'est retournée, on sort avec un code d'erreur
            return -1;
        }
    }

    // Cherchons la valeur de idx en fonction du canal souhaité
    if (channel == 1)
    {
        idx = 3;
    }
    else if (channel == 2)
    {
        idx = 35;
    }
    else
    {
        // Numéro de canal invalide
        return -1;
    }

    // printf("Voltage data: %02x %02x %02x %02x\n", rx_data[idx], rx_data[idx+1], rx_data[idx+2], rx_data[idx+3]);
    voltage = (rx_data[idx] << 24) | (rx_data[idx+1] << 16) | (rx_data[idx+2] << 8) | rx_data[idx+3];
    // printf("Voltage data: %08" PRIx32 "\n", voltage);
    
    return voltage / 10000.0;
}

/**
 * \brief Cette fonction lit un ou plusieurs registres du module JSY.
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont on veut lire le(s) registre(s).
 * \param[in] reg_start_addr Première adresse du registre à lire/
 * \param[in] nb_reg Nombre de registres à lire.
 * \param[out] rx_data Tableau d'octets pour stocker la réponse du module JSY.
 * \return Nombre d'octets reçus du module JSY hors CRC.
*/
int
read_jsy_reg(const uart_port_t uart_num, const uint8_t dev_addr, const uint16_t reg_start_addr, const uint16_t nb_reg, uint8_t* rx_data) {
    
    // Tableau d'octets pour stocker les données à envoyer sur le bus
    uint8_t tx_data[TX_BUF_SIZE];

    // Tableau pour les 2 octets de CRC
    uint8_t crc[2];

    // On construit le tx_data
    tx_data[0] = dev_addr;
    tx_data[1] = 0x03; // Fonction de lecture
    tx_data[2] = (reg_start_addr >> 8) & 0xFF; // Addr du 1er registre (HIGH)
    tx_data[3] = reg_start_addr & 0xFF; // Addr du 1er registre (LOW)
    tx_data[4] = (nb_reg >> 8) & 0xFF; // Nb registres à lire (HIGH)
    tx_data[5] = nb_reg & 0xFF; // Nb registres à lire (LOW)

    // On calcule le CRC
    compute_crc(tx_data, 6, crc);

    // On écrit les 2 octets du CRC à la fin du tableau d'octets de la req
    tx_data[6] = crc[0];
    tx_data[7] = crc[1];

    // On envoie la requête et on quitte si problème
    if (send_data(uart_num, tx_data, 8) < 0) {
        return -1;
    }

    // On essaie de lire la réponse avec un timeout
    const int rx_bytes = uart_read_bytes(uart_num, rx_data, RX_BUF_SIZE, 2000 / portTICK_PERIOD_MS);

    // Si on reçoit des données
    if (rx_bytes > 2) {

        ESP_LOGI("UART", "Read %d bytes", rx_bytes);
        ESP_LOG_BUFFER_HEXDUMP("UART", rx_data, rx_bytes, ESP_LOG_INFO);

        if (check_crc(rx_data, rx_bytes-2)) {
            return rx_bytes - 2;
        }

        printf("Wrong CRC !");

        return -1;
    }

    // Pas de données reçues
    return -1;
}

/**
 * \brief Cette fonction permet de remettre à zéro le cumul d'énergie négative.
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont on veut remettre à zéro le
 *                     cumul d'énergie négative.
 * \param[in] channel Canal dont on veut remettre à zéro le cumul d'énergie
 *                    négative.
 * \return 1 si la remise à zéro est un succès, 0 sinon
 */
uint8_t
reset_negative_energy(const uart_port_t uart_num, const uint8_t dev_addr, int channel) {

    // Adresse du registre à écrire
    uint16_t reg_addr;

    // Tableau d'octets pour stocker les données lues sur le bus
    uint8_t rx_data[RX_BUF_SIZE];

    // Tableau d'octets pour stocker les données à écrire dans le registre
    uint8_t wr_data[TX_BUF_SIZE];
    wr_data[0] = 0x00;
    wr_data[1] = 0x00;
    wr_data[2] = 0x00;
    wr_data[3] = 0x00;

    // Déterminons l'adresse du registre en fonction du canal
    if (channel == 1)
    {
        reg_addr = 0x004D;
    }
    else if (channel == 2)
    {
        reg_addr = 0x0055;
    }
    else
    {
        return 0;
    }

    // On écrit le registre pour remettre à zéro l'énergie négative
    int rx_bytes = write_jsy_reg(uart_num, dev_addr, reg_addr, 2, 4, wr_data, rx_data);

    // Si on a reçut des données, c'est OK
    if (rx_bytes > 0) {
        return rx_bytes;
    }

    // Sinon, on a échoué à remettre l'énergie à z&ro
    return 0;
   
}

/**
 * \brief Cette fonction permet de remettre à zéro le cumul d'énergie positive.
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont on veut remettre à zéro le
 *                     cumul d'énergie positive.
 * \param[in] channel Canal dont on veut remettre à zéro le cumul d'énergie
 *                    positive.
 * \return 1 si la remise à zéro est un succès, 0 sinon
 */
uint8_t
reset_positive_energy(const uart_port_t uart_num, const uint8_t dev_addr, int channel) {

    // Adresse du registre à écrire
    uint16_t reg_addr;

    // Tableau d'octets pour stocker les données lues sur le bus
    uint8_t rx_data[RX_BUF_SIZE];

    // Tableau d'octets pour stocker les données à écrire dans le registre
    uint8_t wr_data[TX_BUF_SIZE];
    wr_data[0] = 0x00;
    wr_data[1] = 0x00;
    wr_data[2] = 0x00;
    wr_data[3] = 0x00;

    // Déterminons l'adresse du registre en fonction du canal
    if (channel == 1)
    {
        reg_addr = 0x004B;
    }
    else if (channel == 2)
    {
        reg_addr = 0x0053;
    }
    else
    {
        return 0;
    }

    // On écrit le registre pour remettre à zéro l'énergie positive
    int rx_bytes = write_jsy_reg(uart_num, dev_addr, reg_addr, 2, 4, wr_data, rx_data);

    // Si on a reçut des données, c'est OK
    if (rx_bytes > 0) {
        return rx_bytes;
    }

    // Sinon, on a échoué à remettre l'énergie à z&ro
    return 0;
   
}

/**
 * \brief Cette fonction envoie un certain nombre d'octets sur le bus UART.
 * 
 * \param[in] uart_num Identifiant de l'UART à utiliser pour envoyer les
 *                     données.
 * \param[in] data Tableau d'octets à envoyer.
 * \param[in] count Nombre d'octets à envoyer.
 * \return Nombre d'octets effectivement envoyés (ou -1 si erreur)
*/
int
send_data(const uart_port_t uart_num, const uint8_t* data, const size_t count)
{
    const int txBytes = uart_write_bytes(uart_num, data, count);
    ESP_LOGI("UART", "Wrote %d bytes", txBytes);
    ESP_LOG_BUFFER_HEXDUMP("UART", data, txBytes, ESP_LOG_INFO);
    return txBytes;
}

/**
 * \brief Cette fonction confirgure la vitesse d'un UART de l'ESP.
 * 
 * \param[in] uart_num Identifiant de l'UART à configurer.
 * \param[out] speed Vitesse à configurer en bauds.
*/
void
set_esp_uart_speed(uart_port_t uart_num, const int speed) {
    const uart_config_t uart_config = {
        .baud_rate = speed,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_DEFAULT,
    };
    uart_param_config(uart_num, &uart_config);
}

/**
 * \brief Cette fonction permet de configurer la vitesse de l'UART d'un module
 *        JSY quelle que soit sa vitesse acteuelle.
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont on veut définir la vitesse.
 * \param[in] speed Vitesse à configurer en bauds.
 * \return 1 si la configuration est un succès, 0 sinon
 */
uint8_t
set_jsy_speed(const uart_port_t uart_num, const uint8_t dev_addr, int speed) {

    // Tableau des différentes vitesses possibles
    int speeds[] = {38400, 19200, 9600, 4800, 2400, 1200};

    // Tableau des différentes valeurs du dernier octet du registre associées
    // aux vitesses du tableau ci-dessus.
    uint8_t speed_vals[] = {8, 7, 6, 5, 4, 3};

    // Index faisant correspondre la vitesse désirée l'élément des tableaux
    // ci-dessous
    uint8_t speed_id = 7;

    // Tableau d'octets pour stocker les données lues sur le bus
    uint8_t rx_data[RX_BUF_SIZE];

    // Cherchons la valeur de cette index
    for (uint8_t i = 0; i < 6; i++)
    {
        if (speed == speeds[i]) {
            speed_id = i;
        }
    }
    
    // Si on n'a pas trouvé l'index, c'est que la vitesse demandée n'est pas
    // valide
    if (speed_id == 7) {
        return 0;
    }

    // Tableau d'octets pour stocker les données à écrire dans le registre
    uint8_t wr_data[TX_BUF_SIZE];
    wr_data[0] = dev_addr;             // 1er octet à écrire
    wr_data[1] = speed_vals[speed_id]; // Valeur de la vitesse souhaitée

    // Il faut commencer par déterminer la vitesse actuelle du JSY
    int current_speed = get_jsy_speed(uart_num, 0x01);

    // Si c'est la bonne vitesse demandée, on quitte
    if (current_speed == speed) {
        return 1;
    }

    // On écrit le registre pour changer la vitesse
    write_jsy_reg(uart_num, dev_addr, 0x0004, 1, 2, wr_data, rx_data);

    // On cherche à nouveau à déterminer la vitesse configurée
    current_speed = get_jsy_speed(uart_num, 0x01);

    // Si c'est la vitesse demandée, on retourne la value 1
    if (current_speed == speed) {
        return 1;
    }

    // Sinon, on a échoué à changer la vitesse
    return 0;
   
}

/**
 * \brief Cette fonction permet de faire la configuration minimale d'un UART de
 * l'ESP.
 * 
 * \param[in] uart_num Identifiant de l'UART à configurer
 * \param[in] tx_pin Numéro de la broche pour l'émission de données
 * \param[in] rx_pin Numéro de la broche pour la réception de données
*/
void
setup_esp_uart(const uart_port_t uart_num, const int tx_pin, const int rx_pin) {
    uart_driver_install(uart_num, RX_BUF_SIZE * 2, 0, 0, NULL, 0);
    uart_set_pin(uart_num, tx_pin, rx_pin, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
}

/**
 * \brief Cette fonction écrit un ou plusieurs registres du module JSY.
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY dont on veut lire le(s) registre(s).
 * \param[in] reg_start_addr Première adresse du registre à écrire.
 * \param[in] nb_reg Nombre de registres à écrire.
 * \param[in] nb_bytes Nombre d'octets à écrire.
 * \param[in] wr_data Tableau d'octets pour stocker les données à écrire dans
 *                    les registres
 * \param[out] rx_data Tableau d'octets pour stocker la réponse du module JSY.
 * \return Nombre d'octets reçus du module JSY hors CRC.
*/
int
write_jsy_reg(const uart_port_t uart_num, const uint8_t dev_addr, const uint16_t reg_start_addr, const uint16_t nb_reg, const uint8_t nb_bytes, uint8_t* wr_data, uint8_t* rx_data) {
    
    // Tableau d'octets pour stocker les données à envoyer sur le bus
    uint8_t tx_data[TX_BUF_SIZE];

    // Tableau pour les 2 octets de CRC
    uint8_t crc[2];

    // Index de remplissage du tx_data
    size_t id_tx = 0;

    // On construit le tx_data
    tx_data[id_tx++] = dev_addr;
    tx_data[id_tx++] = 0x10; // Fonction d'écriture
    tx_data[id_tx++] = (reg_start_addr >> 8) & 0xFF; // Addr du 1er registre (HIGH)
    tx_data[id_tx++] = reg_start_addr & 0xFF; // Addr du 1er registre (LOW)
    tx_data[id_tx++] = (nb_reg >> 8) & 0xFF; // Nb registres à écrire (HIGH)
    tx_data[id_tx++] = nb_reg & 0xFF; // Nb registres à écrire (LOW)
    tx_data[id_tx++] = nb_bytes; // Nb d'octets à écrire
    
    // On complète avec les données à écrire dans les registres
    for (size_t i = 0; i < nb_bytes; i++)
    {
        tx_data[id_tx++] = wr_data[i];
    }
    

    // On calcule le CRC
    compute_crc(tx_data, id_tx, crc);

    // On écrit les 2 octets du CRC à la fin du tableau d'octets de la req
    tx_data[id_tx++] = crc[0];
    tx_data[id_tx++] = crc[1];

    // On envoie la requête et on quitte si problème
    if (send_data(uart_num, tx_data, id_tx) < 0) {
        return -1;
    }

    // On essaie de lire la réponse avec un timeout
    const int rx_bytes = uart_read_bytes(uart_num, rx_data, RX_BUF_SIZE, 2000 / portTICK_PERIOD_MS);

    // Si on reçoit des données
    if (rx_bytes > 2) {

        ESP_LOGI("UART", "Read %d bytes", rx_bytes);
        ESP_LOG_BUFFER_HEXDUMP("UART", rx_data, rx_bytes, ESP_LOG_INFO);

        if (check_crc(rx_data, rx_bytes-2)) {
            return rx_bytes - 2;
        }

        printf("Wrong CRC !");

        return -1;
    }

    // Pas de données reçues
    return -1;
}



/*
Voici les commandes a envoyé au jsy pour faire un reset des compteurs d'énergie (sauf erreur de ma part):
on a 2 bobines qui comptent en + et - donc :
- Positive energy 1 :
commande : 0x01, 0x10, 0x00, 0x4B, 0x00, 0x02, 0x04, 0x00, 0x00, 0x00, 0x00, 0xB6, 0x2C
réponse : 01 10 004B 0002    31 DE
-Negative energy 1 :
commande : 0x01, 0x10, 0x00, 0x4D, 0x00, 0x02, 0x04, 0x00, 0x00, 0x00, 0x00, 0x36, 0x06
réponse : 01 10 004D 0002    D1DF
- Positive energy 2 :
commande : 0x01, 0x10, 0x00, 0x53, 0x00, 0x02, 0x04, 0x00, 0x00, 0x00, 0x00, 0xB6, 0x86
réponse : 01 10 0053 0002    B1D9
-Negative energy 2 :
commande : 0x01, 0x10, 0x00, 0x55, 0x00, 0x02, 0x04, 0x00, 0x00, 0x00, 0x00, 0x36, 0xAC
réponse : 01 10 0055 0002    51D8
*/