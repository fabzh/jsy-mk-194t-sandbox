/* UART asynchronous example, that uses separate RX and TX tasks

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "jsy.h"

void app_main(void);
void init(const uart_port_t, const uint8_t);

/**
 * \brief Fonction principale de l'application.
*/
void
app_main(void) {
    // Délai entre 2 mesures
    const TickType_t xDelay = 10000 / portTICK_PERIOD_MS;

    init(UART_NUM_1, 0x01);

    // Tableau d'octets pour stocker les données de mesure
    uint8_t rx_data[RX_BUF_SIZE];
    
    for( ;; )

    {
        uint8_t nb_bytes = get_measure(UART_NUM_1, 0x01, rx_data);

        if (nb_bytes > 0)
        {
            float voltage = get_voltage(UART_NUM_1, 0x01, rx_data, 1, 0);
            printf("Voltage channel 1: %f V\n", voltage);

            float current = get_current(UART_NUM_1, 0x01, rx_data, 1, 0);
            printf("Current channel 1: %f A\n", current);

            float power = get_power(UART_NUM_1, 0x01, rx_data, 1, 0);
            printf("Power channel 1: %f W\n", power);

            float positive_energy = get_positive_energy(UART_NUM_1, 0x01, rx_data, 1, 0);
            printf("Positive energy channel 1: %f kWh\n", positive_energy);

            float power_factor = get_power_factor(UART_NUM_1, 0x01, rx_data, 1, 0);
            printf("Power factor channel 1: %f\n", power_factor);

            float negative_energy = get_negative_energy(UART_NUM_1, 0x01, rx_data, 1, 0);
            printf("Negative energy channel 1: %f kWh\n", negative_energy);

            float frequency = get_frequency(UART_NUM_1, 0x01, rx_data, 0);
            printf("Frequency channel 1: %f Hz\n", frequency);
        }
        
        vTaskDelay( xDelay );

    }
}

/**
 * \brief Cette fonction prend en charge toutes les tâches d'initialisation
 *        devant être réalisées avant que le programme n'entre dans son mode de
 *        fonctionnement nominal.
 * 
 * \param[in] uart_num Identifiant de l'UART via lequel le module JSY est
 *                     accessible.
 * \param[in] dev_addr Adresse du module JSY que l'on veut initialiser.
*/
void
init(const uart_port_t uart_num, const uint8_t dev_addr) {
    printf("Init !!\n");

    setup_esp_uart(UART_NUM_1, TXD_PIN, RXD_PIN);
    
    const int speed = 38400;

    // De manière inexpliquée quand la vitesse initiale était 38400, il faut
    // s'y reprendre à 2 fois pour redéfinir la nouvelle vitesse...
    if (! set_jsy_speed(uart_num, dev_addr, speed)) {
        set_jsy_speed(uart_num, dev_addr, speed);
    }

    // Remise à zéro de l'énergie positive
    reset_positive_energy(uart_num, dev_addr, 1);
    reset_positive_energy(uart_num, dev_addr, 2);

    // Remise à zéro de l'énergie négative
    reset_negative_energy(uart_num, dev_addr, 1);
    reset_negative_energy(uart_num, dev_addr, 2);
}
