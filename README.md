# JSY MK 194T Sandbox

## Compilation

```shell
$ source ~/esp/esp-idf/export.sh
$ idf.py set-target esp32s3
$ idf.py menuconfig
$ idf.py build
$ idf.py flash -p /dev/ttyUSB0 && idf.py monitor -p /dev/ttyUSB0
```

Press `Ctrl-]` to leave monitor.