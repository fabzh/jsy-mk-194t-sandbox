

| Index | Description |
|-------|-------------|
| 0	 | ID du module sur le bus |
| 1	 | Code fonction |
| 2	 | Nb octets retournés |
| 3	 | Voltage channel 1 |
| 4	 | Voltage channel 1 |
| 5	 | Voltage channel 1 |
| 6	 | Voltage channel 1 |
| 7	 | Current channel 1 |
| 8	 | Current channel 1 |
| 9	 | Current channel 1 |
| 10 | 	Current channel 1 |
| 11 | 	Power channel 1 |
| 12 | 	Power channel 1 |
| 13 | 	Power channel 1 |
| 14 | 	Power channel 1 |
| 15 | 	Positive energy channel 1 |
| 16 | 	Positive energy channel 1 |
| 17 | 	Positive energy channel 1 |
| 18 | 	Positive energy channel 1 |
| 19 | 	Power factor channel 1 |
| 20 | 	Power factor channel 1 |
| 21 | 	Power factor channel 1 |
| 22 | 	Power factor channel 1 |
| 23 | 	Negative energy channel 1 |
| 24 | 	Negative energy channel 1 |
| 25 | 	Negative energy channel 1 |
| 26 | 	Negative energy channel 1 |
| 27 | 	Power direction channel 1 |
| 28 | 	Power direction channel 2 |
| 29 | 	N/A |
| 30 | 	N/A |
| 31 | 	Frenquency |
| 32 | 	Frenquency |
| 33 | 	Frenquency |
| 34 | 	Frenquency |
| 35 | 	Voltage channel 2 |
| 36 | 	Voltage channel 2 |
| 37 | 	Voltage channel 2 |
| 38 | 	Voltage channel 2 |
| 39 | 	Current channel 2 |
| 40 | 	Current channel 2 |
| 41 | 	Current channel 2 |
| 42 | 	Current channel 2 |
| 43 | 	Power channel 2 |
| 44 | 	Power channel 2 |
| 45 | 	Power channel 2 |
| 46 | 	Power channel 2 |
| 47 | 	Positive energy channel 2 |
| 48 | 	Positive energy channel 2 |
| 49 | 	Positive energy channel 2 |
| 50 | 	Positive energy channel 2 |
| 51 | 	Power factor channel 2 |
| 52 | 	Power factor channel 2 |
| 53 | 	Power factor channel 2 |
| 54 | 	Power factor channel 2 |
| 55 | 	Negative energy channel 2 |
| 56 | 	Negative energy channel 2 |
| 57 | 	Negative energy channel 2 |
| 58 | 	Negative energy channel 2 |